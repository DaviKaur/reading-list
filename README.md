Recommendations are welcome!!

<h3>Currently reading</h3>

* Sapiens - Yuval Noah Harari
* The Hidden Life of trees - Peter Wohlleben

<h3>Reading Challenge</h3>

* Re-read - Leaves of grass - Walt Whitman
* Re-read - Freedom From the Known - J. Krishnamurti
 
<h3>Next reads</h3>

* Homo Duos - Yuval Noah Harari
* 21 Lessons for 21st Centuray - Yuval Noah Harari
* Life Span - David A. Sinclair

<h3>Recommendations</h3>

* Freedom from the Known - J.Krishnamurti 

<h3>Reading List</h3>

* Happy is the Man who is Nothing - J. Krishnamurti
* Animal Farm - George Orwell
* Leaves of Grass - Walt Whitman 
* Freedom from the Known - J. Krishnamurti
* Education and the significance of Life - J. Krishnamurti
* Chup Preet da Shehanshaah Beupaari - Prof. Puran Singh (Punjabi)
* Trel Tupke - Bhai Vir Singh Ji (Punjabi)
* Nargas - Prof. Puran Singh
* Sundari - Bhai Vir Singh Ji (Punjabi)
* Rana Bhabaur - Bhai Vir Singh Ji (Punjabi)
* Collection of stories by - Sadat Hassan Manto

<h3>Data Science Courses</h3>

* https://www.coursera.org/professional-certificates/ibm-data-science
